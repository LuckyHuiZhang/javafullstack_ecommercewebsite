package com.huiproject.springbootecommerce.dto;

import com.huiproject.springbootecommerce.entity.Address;
import com.huiproject.springbootecommerce.entity.Customer;
import com.huiproject.springbootecommerce.entity.Order;
import com.huiproject.springbootecommerce.entity.OrderItem;
import lombok.Data;

import java.util.Set;

@Data
public class Purchase {
    private Customer customer;
    private Address shippingAddress;
    private Address billingAddress;
    private Order order;
    private Set<OrderItem> orderItems;
}
