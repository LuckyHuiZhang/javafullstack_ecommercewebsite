package com.huiproject.springbootecommerce.dao;

import com.huiproject.springbootecommerce.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestParam;


@RepositoryRestResource //Spring data REST will only expose aREST endpoint if uses annotation
public interface ProductRepository extends JpaRepository<Product, Long> {

    //add query method for dynamic searching, Spring Data REST automatically exposes endpoint
    Page<Product> findByCategoryId(@RequestParam("id") Long id, Pageable pageable);

    //add query method for keyword searching
    Page<Product> findByNameContaining(@RequestParam("name") String name, Pageable pageable);

}
