package com.huiproject.springbootecommerce.service;

import com.huiproject.springbootecommerce.dto.PaymentInfo;
import com.huiproject.springbootecommerce.dto.Purchase;
import com.huiproject.springbootecommerce.dto.PurchaseResponse;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;

public interface CheckoutService {
    PurchaseResponse placeOrder(Purchase purchase);
    PaymentIntent createPaymentIntent(PaymentInfo paymentInfo) throws StripeException;
}
