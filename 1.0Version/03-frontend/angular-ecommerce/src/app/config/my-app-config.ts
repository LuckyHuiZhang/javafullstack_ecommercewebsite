export default {
  oidc: {
    clientId: '0oa45xljn9SKHB3015d7',
    issuer: 'https://dev-34574429.okta.com/oauth2/default',
    // clientId: '0oa483ykhaN8D0cmp5d7',
    // issuer: 'https://dev-34574429.okta.com/oauth2/default',
    redirectUri: 'https://localhost:4200/login/callback',
    scopes: ['openid', 'profile', 'email'],
  },
};
